# fibonacci.py

def generate_fibonacci(n):
    fib_sequence = [0, 1]
    while fib_sequence[-1] + fib_sequence[-2] < n:
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
    return fib_sequence

def main():
    n = int(input("Enter the maximum number for Fibonacci sequence: "))
    fib_sequence = generate_fibonacci(n)
    print("Fibonacci sequence up to", n, ":", fib_sequence)

if __name__ == "__main__":
    main()
